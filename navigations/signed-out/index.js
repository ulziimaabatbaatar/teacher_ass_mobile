import { StyleSheet } from "react-native";
import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import PropTypes from "prop-types";
import Login from "../../screens/signed-out/login";
import SendOTP from "../../screens/signed-out/sendOTP";

const SignedOut = ({ setIsSignedIn }) => {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen options={{ headerShown: false }} name="Login">
        {(props) => <Login {...props} setIsSignedIn={setIsSignedIn} />}
      </Stack.Screen>
      <Stack.Screen
        options={{ headerShown: false }}
        name="SendOTP"
        component={SendOTP}
        initialParams={{ setIsSignedIn }}
      />
    </Stack.Navigator>
  );
};

SignedOut.propTypes = {
  setIsSignedIn: PropTypes.func,
};

export default SignedOut;

const styles = StyleSheet.create({});
