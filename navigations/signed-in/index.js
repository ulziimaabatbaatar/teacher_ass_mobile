import React from "react";
import { StyleSheet } from "react-native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MainTabs from "../../screens/signed-in/MainTabs";

const SignedIn = () => {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator initialRouteName="HomeStack">
      <Stack.Screen
        options={{ headerShown: false }}
        name="HomeStack"
        component={MainTabs}
      />
    </Stack.Navigator>
  );
};

export default SignedIn;

const styles = StyleSheet.create({});
