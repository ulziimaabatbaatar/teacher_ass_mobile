import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { StyleSheet } from "react-native";
import SignedIn from "./navigations/signed-in";
import { AuthProvider } from "./src/auth/context";
import SignedOut from "./navigations/signed-out";

export default function App() {
  const [isSignedIn, setIsSignedIn] = useState(false);

  return (
    <AuthProvider>
      <NavigationContainer>
        {/* <SignedIn /> */}
        {isSignedIn ? (
          <SignedIn />
        ) : (
          <SignedOut setIsSignedIn={setIsSignedIn} />
        )}
      </NavigationContainer>
    </AuthProvider>
  );
}

const styles = StyleSheet.create({});
