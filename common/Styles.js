import { StyleSheet } from "react-native";
import Colors from "../constants/Colors";

const Styles = StyleSheet.create({
  boldText: {
    fontWeight: "bold",
  },
  container: {
    flex: 1,
  },
  contentContainerStyle: {
    paddingBottom: 200,
  },
  contentContainerStyle2: {
    paddingBottom: 100,
  },
  rowView: {
    alignItems: "center",
    flexDirection: "row",
  },
  separator: {
    backgroundColor: Colors.gray,
    height: 0.3,
    opacity: 0.8,
    width: "100%",
  },
});

export default Styles;
