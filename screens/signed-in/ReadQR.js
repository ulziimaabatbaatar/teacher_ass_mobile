import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useEffect, useState } from "react";
import { BarCodeScanner } from "expo-barcode-scanner";
import * as Location from "expo-location";
import Colors from "../../constants/Colors";
import { axiosGeneral, endpoints } from "../../src/utils/axios";

export default function App() {
  const [hasPermission, setHasPermission] = useState(false);
  const [scanData, setScanData] = useState();
  const [location, setLocation] = useState();

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  if (!hasPermission) {
    return (
      <View style={styles.container}>
        <Text>Please grant camera permissions to app.</Text>
      </View>
    );
  }

  async function getPermissions() {
    const { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      // console.log("Please grant location permissions");
      return;
    }

    const currentLocation = await Location.getCurrentPositionAsync({});
    setLocation(currentLocation);
    // console.log("Location:", currentLocation);
  }

  const handleBarCodeScanned = async ({ type, data }) => {
    setScanData(data);

    await getPermissions();

    const x = JSON.parse(data);

    const req_body = {
      qr_containing_text: x.qrContainingText,
      attendance_id: x.attendanceId,
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    };

    console.log(req_body, "req body");

    const URL = endpoints.student.attendance;

    try {
      const response = await axiosGeneral?.post(URL, req_body);
      if (response?.status === 200 || response?.status === 201) {
        console.log("success");
      }
    } catch (error) {
      console.error(error);
      console.log("fail!!!!!!!!!!!!!!!!!");
    }
  };

  return (
    <View style={styles.container}>
      <BarCodeScanner
        style={StyleSheet.absoluteFillObject}
        onBarCodeScanned={scanData ? undefined : handleBarCodeScanned}
      />
      {scanData && (
        <TouchableOpacity onPress={() => setScanData(undefined)}>
          <View style={styles.btn}>
            <Text style={styles.btnText}>Дахиж уншуулах</Text>
          </View>
        </TouchableOpacity>
      )}

      <StatusBar />
    </View>
  );
}

const styles = StyleSheet.create({
  btn: {
    alignItems: "center",
    backgroundColor: Colors.main,
    borderRadius: 10,
    border: "none",
    flexDirection: "row",
    justifyContent: "center",
    padding: 15,
  },

  btnText: { color: Colors.white, fontSize: 14, fontWeight: "600" },

  container: {
    alignItems: "center",
    backgroundColor: Colors.white,
    flex: 1,
    justifyContent: "center",
  },
});
