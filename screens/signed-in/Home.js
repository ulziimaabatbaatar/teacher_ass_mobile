import React from "react";
import { Text, View, StyleSheet, Image, ScrollView } from "react-native";
import { Card } from "react-native-paper";
import { LinearGradient } from "expo-linear-gradient";
import Colors from "../../constants/Colors";
import { useGetSubjects } from "../../src/api/subject";
import { useAuthContext } from "../../src/auth/hooks/use-auth-context";

const subjectList = [
  {
    id: 1,
    subject_name: "Mathematics",
    subject_code: "F.CS393",
  },
  {
    id: 2,
    subject_name: "History",
    subject_code: "F.CS393",
  },
  {
    id: 3,
    subject_name: "English Literature",
    subject_code: "F.CS393",
  },
  {
    id: 4,
    subject_name: "Physics",
    subject_code: "F.CS393",
  },
  {
    id: 5,
    subject_name: "Biology",
    subject_code: "F.CS393",
  },
  {
    id: 6,
    subject_name: "Chemistry",
    subject_code: "F.CS393",
  },
  {
    id: 7,
    subject_name: "Computer Science",
    subject_code: "F.CS393",
  },
  {
    id: 8,
    subject_name: "Geography",
    subject_code: "F.CS393",
  },
  {
    id: 9,
    subject_name: "Economics",
    subject_code: "F.CS393",
  },
  {
    id: 10,
    subject_name: "Psychology",
    subject_code: "F.CS393",
  },
];

export default function HomeScreen() {
  const { user } = useAuthContext();

  const {
    subjects,
    subjectsError,
    subjectsFetch,
    subjectsLoading,
    subjectsValidating,
    subjectsPagination,
    subjectsEmpty,
  } = useGetSubjects();

  const renderBody =
    subjects && subjects.length > 0 ? (
      <>
        {subjects.map((item, index) => (
          <Card key={index} style={styles.card}>
            <Text style={{ color: Colors.black }}>
              {item.subject_name} - {item.subject_code}
              {item.subject_schedules.map((sch) => (
                <View>
                  {sch.lesson_type.lesson_type_name} - {sch.schedule_name}
                </View>
              ))}
            </Text>
          </Card>
        ))}
      </>
    ) : null;

  console.log(subjects, "subjects");

  return (
    <ScrollView style={styles.container}>
      <View style={styles.firstContainerView}>
        <Header />

        <LinearGradient
          colors={["#6E7FE4", "#e46e7f"]}
          style={styles.button}
          start={[1, 0]}
          end={[0, 0]}
        >
          <Text style={styles.cardText}>Hi, Ziimaa! {user.student_code}</Text>
        </LinearGradient>
      </View>

      <View style={styles.secondContainerView}>
        <Text style={styles.yourLessonsText}>Таны хичээлүүд</Text>

        {/* {subjectList.map((subject) => (
          <Card style={styles.card}>
            <Text>
              {subject.subject_name} - {subject.subject_code}
            </Text>
          </Card>
        ))} */}

        {renderBody}
      </View>
    </ScrollView>
  );
}

const Header = () => (
  <View style={styles.headerContainer}>
    <View style={styles.imgView}>
      <Image
        alt="otp"
        // eslint-disable-next-line global-require
        source={require("../../assets/logo/logo_irtsly.png")}
        style={styles.headerImg}
      />
    </View>

    <Image
      alt="notif"
      // eslint-disable-next-line global-require
      source={require("../../assets/icons/notif.png")}
      style={styles.notifIcon}
    />
  </View>
);

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    borderRadius: 15,
    padding: 45,
  },
  card: {
    backgroundColor: Colors.white,
    borderRadius: 20,
    padding: 20,
  },

  cardText: {
    color: Colors.white,
    fontSize: 20,
    fontWeight: "700",
    // fontWeight: 800,
  },

  container: {
    backgroundColor: Colors.backgroundHome,
  },
  firstContainerView: {
    backgroundColor: Colors.white,
    paddingBottom: 20,
    paddingHorizontal: 20,
  },
  headerContainer: {
    borderRadius: 30,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 20,
  },
  headerImg: { height: 30, width: 100 },

  notifIcon: {
    height: 25,
    width: 25,
  },

  secondContainerView: {
    gap: 20,
    marginBottom: 20,
    paddingHorizontal: 20,
  },
  yourLessonsText: {
    color: Colors.disabledText,
    fontWeight: 500,
    paddingTop: 10,
  },
});
