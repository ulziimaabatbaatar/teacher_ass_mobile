import PropTypes from "prop-types";

import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { SafeAreaView } from "react-native-safe-area-context";
import SettingsScreen from "./Settings";
import Icon, { Icons } from "../../components/Icons";
import Colors from "../../constants/Colors";
import HomeScreen from "./Home";
import ReadQRScreen from "./ReadQR";

const TabArr = [
  {
    route: "HomeTab",
    label: "Home",
    type: Icons.Feather,
    icon: "home",
    component: HomeScreen,
    color: Colors.white,
    alphaClr: Colors.black,
  },
  {
    route: "ReadQR",
    label: "ReadQR",
    type: Icons.Ionicons,
    icon: "qr-code-outline",
    component: ReadQRScreen,
    color: Colors.white,
    alphaClr: Colors.black,
    raise: true,
  },
  {
    route: "Settings",
    label: "Settings",
    type: Icons.Ionicons,
    icon: "person-outline",
    component: SettingsScreen,
    color: Colors.white,
    alphaClr: Colors.black,
  },
];

const Tab = createBottomTabNavigator();

const TabButton = ({ item, onPress, accessibilityState }) => {
  const focused = accessibilityState.selected;

  const buttonStyle = {
    padding: 10,
    borderBottomWidth: focused ? 2 : 0,
    borderBottomColor: focused ? Colors.main : "transparent",
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={styles.container}
    >
      <View>
        <View style={buttonStyle}>
          <Icon
            type={item.type}
            name={item.icon}
            color={focused ? Colors.main : Colors.lighter}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

TabButton.propTypes = {
  item: PropTypes.object,
  onPress: PropTypes.func,
  accessibilityState: PropTypes.shape({
    selected: PropTypes.bool,
  }),
};

export default function MainTabs() {
  return (
    <SafeAreaView style={styles.main}>
      <Tab.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
          tabBarStyle: {
            height: 55,
            position: "absolute",
            marginLeft: 45,
            marginRight: 45,
            backgroundColor: "#fff",
            borderRadius: 30,
            bottom: 20,
            left: "10%",
            right: "10%",
            alignItems: "center",
            justifyContent: "space-evenly",
          },
        }}
      >
        {TabArr.map((item, index) => (
          <Tab.Screen
            key={index}
            name={item.route}
            component={item.component}
            options={{
              tabBarShowLabel: false,
              tabBarButton: (props) => <TabButton {...props} item={item} />,
            }}
          />
        ))}
      </Tab.Navigator>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    height: 50,
    justifyContent: "center",
    marginHorizontal: 10,
  },
  main: { flex: 1 },
});
