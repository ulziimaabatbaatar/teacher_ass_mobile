import React from "react";
import { Button, Text, View, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import Colors from "../../constants/Colors";

export default function SettingsScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>Settings screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate("Details")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: Colors.white,
    flex: 1,
    justifyContent: "center",
  },
});

SettingsScreen.propTypes = {
  navigation: PropTypes.object,
};
