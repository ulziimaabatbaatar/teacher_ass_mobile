import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Dimensions,
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";
import { Controller, useForm } from "react-hook-form";
import PropTypes from "prop-types";
import { useNavigation } from "@react-navigation/native";
import { TextInput } from "react-native-paper";
import Colors from "../../constants/Colors";
import { useAuthContext } from "../../src/auth/hooks/use-auth-context";

const SendOTP = ({ route }) => {
  const { login } = useAuthContext();
  const { email, setIsSignedIn } = route.params;

  const [passwordVisible, setPasswordVisible] = useState(true);

  const [errorMsg, setErrorMsg] = useState("");

  const navigation = useNavigation();

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
    watch,
    reset,
  } = useForm({ mode: "onBlur", defaultValues: { email: "", password: "" } });

  const values = watch();

  const onSubmit = handleSubmit(async (data) => {
    const req_body = {
      email,
      password: data?.password,
    };
    try {
      const response = await login?.(req_body);
      if (response) {
        setIsSignedIn(true);
      }
    } catch (error) {
      console.error(error);
      reset();
      setErrorMsg(typeof error === "string" ? error : error.message);
    }
  });

  const renderHeader = (
    <View style={styles.header}>
      <View style={styles.imgView}>
        <Image
          alt="otp"
          // source={require("../../assets/illustrations/email.jpg")}

          // eslint-disable-next-line global-require
          source={require("../../assets/logo/logo_irtsly.png")}
          style={styles.headerImg}
        />
      </View>
    </View>
  );

  const renderForm = (
    <View style={styles.form}>
      <View style={styles.input}>
        <Text>Баталгаажуулах код</Text>

        <Controller
          control={control}
          name="password"
          render={({ field: { onChange, value, onBlur } }) => (
            <TextInput
              placeholder="И-мэйл руу явуулсан код"
              underlineColor="transparent"
              secureTextEntry={passwordVisible}
              value={value}
              onBlur={onBlur}
              onChangeText={onChange}
              cursorColor={Colors.main}
              theme={{
                colors: {
                  primary: "transparent",
                  // underlineColor: "transparent",
                },
              }}
              right={
                <TextInput.Icon
                  icon={passwordVisible ? "eye" : "eye-off"}
                  onPress={() => setPasswordVisible(!passwordVisible)}
                />
              }
              style={styles.inputControl}
            />
          )}
        />
      </View>
      <TouchableOpacity onPress={handleSubmit(onSubmit)}>
        <View style={styles.btn}>
          <Text style={styles.btnText}>Нэвтрэх</Text>
        </View>
      </TouchableOpacity>
    </View>
  );

  const handleFooterClicked = () => {
    navigation.navigate("Login");
  };

  const handleBackClicked = () => {
    navigation.navigate("Login");
  };

  const renderFooter = (
    <View style={styles.footer}>
      <TouchableOpacity onPress={() => handleFooterClicked()}>
        <Text>
          Баталгаажуулах код аваагүй юу?{" "}
          <Text style={styles.sendAgainText}>Дахиж явуулах</Text>
        </Text>
      </TouchableOpacity>
    </View>
  );

  return (
    <SafeAreaView style={styles.safeArea}>
      <TouchableOpacity onPress={() => handleBackClicked()}>
        <Image
          alt="otp"
          // source={require("../../assets/illustrations/email.jpg")}
          // eslint-disable-next-line global-require
          source={require("../../assets/icons/back.png")}
          style={styles.backIcon}
        />
      </TouchableOpacity>

      <KeyboardAvoidingView>
        <ScrollView automaticallyAdjustKeyboardInsets>
          <View style={styles.container}>
            {renderHeader}
            {renderForm}
            {renderFooter}
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

SendOTP.propTypes = {
  route: PropTypes.object,
  setIsSignedIn: PropTypes.func,
};

export default SendOTP;

const screenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  backIcon: { height: 30, marginLeft: 10, width: 30 },

  btn: {
    alignItems: "center",
    backgroundColor: Colors.main,
    borderRadius: 10,
    border: "none",
    flexDirection: "row",
    justifyContent: "center",
    paddingVertical: 12,
  },
  btnText: { color: Colors.white, fontSize: 14, fontWeight: "600" },

  container: {
    backgroundColor: Colors.white,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    flex: 1,
    marginBottom: 0,
    marginTop: "80%",
    paddingBottom: screenHeight * 0.3,
    paddingHorizontal: 24,
  },
  footer: {
    alignItems: "center",
    marginTop: 10,
  },

  form: { gap: 20 },
  header: {
    marginTop: "10%",
  },

  headerImg: { height: 40, width: 140 },
  imgView: {
    alignSelf: "center",
    backgroundColor: Colors.white,
    borderRadius: 16,
    padding: 20,
  },
  input: { gap: 8 },
  inputControl: {
    backgroundColor: Colors.background,
    border: "none",
    borderRadius: 12,
    paddingHorizontal: 18,
  },
  safeArea: {
    backgroundColor: Colors.main,
    flex: 1,
  },
  sendAgainText: { color: Colors.main },
});
