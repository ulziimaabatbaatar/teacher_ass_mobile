import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  Button,
} from "react-native";
import { TextInput } from "react-native-paper";

import { SafeAreaView } from "react-native-safe-area-context";
import { useForm, Controller } from "react-hook-form";
import PropTypes from "prop-types";
import { useNavigation } from "@react-navigation/native";
import Colors from "../../constants/Colors";
import { useAuthContext } from "../../src/auth/hooks/use-auth-context";
import { useBoolean } from "../../src/hooks/use-boolean";
import CustomAlert from "../../components/custom-alert";

const Login = ({ setIsSignedIn }) => {
  const { sendOTP } = useAuthContext();

  const [errorMsg, setErrorMsg] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const simpleAlertHandler = () => {
    setModalVisible(true); // Show the modal when the button is pressed
  };

  const handleCancel = () => {
    console.log("Cancelled");
    setModalVisible(false); // Hide the modal when cancelled
  };

  const openSuccessDialog = useBoolean();

  const navigation = useNavigation();

  const {
    control,
    handleSubmit,
    formState: { errors, isValid },
    watch,
    reset,
  } = useForm({ mode: "onBlur", defaultValues: { email: "", password: "" } });

  const values = watch();

  const onSubmitEmail = handleSubmit(async (data) => {
    const emailPrefix = data?.email.split("@")[0];

    const req_body = {
      student_code: emailPrefix,
      email: data.email,
    };

    try {
      const response = await sendOTP?.(req_body);
      if (response?.status === 200 || response?.status === 201) {
        openSuccessDialog.onTrue();
        navigation.navigate("SendOTP", {
          email: data.email,
          // eslint-disable-next-line object-shorthand
          setIsSignedIn: setIsSignedIn,
        });
        // setShowSecondInput(true);
      }
    } catch (error) {
      console.error(error);
      reset();
      setErrorMsg(typeof error === "string" ? error : error.message);
    }
  });

  const renderHeader = (
    <View style={styles.header}>
      <View style={styles.imgView}>
        <Image
          alt="otp"
          // eslint-disable-next-line global-require
          source={require("../../assets/logo/logo_irtsly.png")}
          style={styles.headerImg}
        />
      </View>
    </View>
  );

  const renderForm = (
    <View style={styles.form}>
      <View style={styles.input}>
        <Text>И-мэйл</Text>
        <Controller
          control={control}
          name="email"
          render={({ field: { onChange, value, onBlur } }) => (
            <TextInput
              placeholder="Оюутны и-мэйл"
              underlineColor="transparent"
              value={value}
              onBlur={onBlur}
              onChangeText={onChange}
              cursorColor={Colors.main}
              theme={{
                colors: {
                  primary: "transparent",
                  // underlineColor: "transparent",
                },
              }}
              left={<TextInput.Icon icon="email" />}
              style={styles.inputControl}
            />
          )}
        />
      </View>
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Button
          onPress={() => simpleAlertHandler()}
          title="Simple Alert"
          color="#90A4AE"
        />
        <CustomAlert
          isVisible={modalVisible}
          onCancel={handleCancel}
          message="I am two option alert. Do you want to cancel me ?"
        />
      </View>

      <TouchableOpacity onPress={handleSubmit(onSubmitEmail)}>
        <View style={styles.btn}>
          <Text style={styles.btnText}>Баталгаажуулах код явуулах</Text>
        </View>
      </TouchableOpacity>
    </View>
  );

  return (
    <SafeAreaView style={styles.safeArea}>
      <KeyboardAvoidingView>
        <ScrollView automaticallyAdjustKeyboardInets>
          <View style={styles.container}>
            {/* {renderSuccessDialog} */}
            {renderHeader}
            {renderForm}
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

Login.propTypes = {
  setIsSignedIn: PropTypes.func,
};

export default Login;

const screenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  btn: {
    alignItems: "center",
    backgroundColor: Colors.main,
    borderRadius: 10,
    border: "none",
    flexDirection: "row",
    justifyContent: "center",
    paddingVertical: 12,
  },

  btnText: { color: Colors.white, fontSize: 14, fontWeight: "600" },
  container: {
    backgroundColor: Colors.white,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    flex: 1,
    marginBottom: 0,
    marginTop: "80%",
    paddingBottom: screenHeight * 0.5,
    paddingHorizontal: 24,
  },
  form: { gap: 20 },
  header: {
    marginTop: "10%",
  },
  headerImg: { height: 40, width: 140 },
  imgView: {
    alignSelf: "center",
    backgroundColor: Colors.white,
    borderRadius: 16,
    padding: 20,
  },
  input: { gap: 8 },
  inputControl: {
    backgroundColor: Colors.background,
    border: "none",
    borderRadius: 12,
    paddingHorizontal: 18,
  },
  safeArea: {
    backgroundColor: Colors.main,
    flex: 1,
  },
});
