import useSWR from "swr";
import { useMemo } from "react";

import { axiosGeneral, endpoints, fetcher } from "../utils/axios";

// ----------------------------------------------------------------------

export function useGetSubjects() {
  const URL = endpoints.subject.list;

  const { data, isLoading, error, isValidating, mutate } = useSWR(
    [URL, axiosGeneral, "get"],
    fetcher,
    {
      shouldRetryOnError: false,
    }
  );

  console.log(data, "data here");

  const memoizedValue = useMemo(
    () => ({
      subjects: data || [],
      subjectsError: error,
      subjectsFetch: mutate,
      subjectsLoading: isLoading,
      subjectsValidating: isValidating,
      subjectsPagination: data?.pagination,
      subjectsEmpty: !isLoading && !data?.length,
    }),
    [data, error, isLoading, isValidating, mutate]
  );

  return memoizedValue;
}
