import useSWR from "swr";
import { useMemo } from "react";
import { axiosGeneral, endpoints, fetcher } from "../utils/axios";

// ----------------------------------------------------------------------

export function useGetStudentOtp(request_body) {
  const URL = endpoints.student.otp;

  const { data, isLoading, error, isValidating, mutate } = useSWR(
    [URL, axiosGeneral, "post", request_body],
    fetcher,
    {
      shouldRetryOnError: false,
    }
  );

  const memoizedValue = useMemo(
    () => ({
      student: data || [],
    }),
    [data]
  );

  return memoizedValue;
}
