import axios from "axios";

// eslint-disable-next-line import/no-unresolved
import { GENERAL_API, HOST_API, LOGIN_API } from "@env";

// ----------------------------------------------------------------------

const axiosInstance = axios.create({ baseURL: HOST_API });

axiosInstance.interceptors.response.use(
  (res) => res,
  (error) =>
    Promise.reject(
      (error.response && error.response.data) || "Something went wrong"
    )
);

export default axiosInstance;

// ----------------------------------------------------------------------

export const axiosLogin = axios.create({ baseURL: LOGIN_API });

axiosLogin.interceptors.response.use(
  (res) => res,
  (error) =>
    Promise.reject(
      (error.response && error.response.data) || "Something went wrong"
    )
);

// ----------------------------------------------------------------------

export const axiosGeneral = axios.create({ baseURL: GENERAL_API });

axiosGeneral.interceptors.response.use(
  (res) => res,
  (error) =>
    Promise.reject(
      (error.response && error.response.data) || "Something went wrong"
    )
);

// ----------------------------------------------------------------------

export const fetcher = async (args) => {
  const [url, customAxios, method, body, config] = Array.isArray(args)
    ? args
    : [args];

  const response = await customAxios[method](url, body, { ...config });

  return response?.data;
};

// ----------------------------------------------------------------------

export const endpoints = {
  auth: {
    me: "/get_auth_info_student",
    otp: "/send_otp_student",
  },
  student: {
    attendance: "/register_attendance_in_mobile",
  },

  subject: {
    list: "/get_students_subjects",
  },
  // student: {
  //   otp: "/send_otp_student",
  // },
};
