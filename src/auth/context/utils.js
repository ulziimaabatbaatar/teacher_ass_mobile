import AsyncStorage from "@react-native-async-storage/async-storage";
import { Base64 } from "js-base64";
import axios, { axiosGeneral } from "../../utils/axios";
// ----------------------------------------------------------------------

function jwtDecode(token) {
  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  const jsonPayload = decodeURIComponent(
    Base64.atob(base64)
      .split("")
      .map((c) => `%${`00${c.charCodeAt(0).toString(16)}`.slice(-2)}`)
      .join("")
  );

  return JSON.parse(jsonPayload);
}

// ----------------------------------------------------------------------

export const isValidToken = (accessToken) => {
  if (!accessToken) {
    return false;
  }

  const decoded = jwtDecode(accessToken);

  const currentTime = Date.now() / 1000;

  return decoded.exp > currentTime;
};

// ----------------------------------------------------------------------

export const tokenExpired = (exp) => {
  let expiredTimer;

  const currentTime = Date.now();
  const timeLeft = exp * 1000 - currentTime;

  clearTimeout(expiredTimer);

  expiredTimer = setTimeout(async () => {
    alert("Token expired");
    await AsyncStorage.removeItem("accessToken");
    // Handle the navigation to login screen or logout user
  }, timeLeft);
};

// ----------------------------------------------------------------------

export const setSession = async (accessToken) => {
  if (accessToken) {
    await AsyncStorage.setItem("accessToken", accessToken);

    axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
    axiosGeneral.defaults.headers.common.Authorization = `Bearer ${accessToken}`;

    const { exp } = jwtDecode(accessToken);
    tokenExpired(exp);
  } else {
    await AsyncStorage.removeItem("accessToken");

    delete axios.defaults.headers.common.Authorization;
    delete axiosGeneral.defaults.headers.common.Authorization;
  }
};
