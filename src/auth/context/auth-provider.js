import PropTypes from "prop-types";

import { useMemo, useEffect, useReducer, useCallback } from "react";

import AsyncStorage from "@react-native-async-storage/async-storage";
import { AuthContext } from "./auth-context";
import { setSession, isValidToken } from "./utils";
import axios, { axiosGeneral, axiosLogin, endpoints } from "../../utils/axios";

// ----------------------------------------------------------------------

// NOTE:
// We only build demo at basic level.
// Customer will need to do some extra handling yourself if you want to extend the logic and other features...

// ----------------------------------------------------------------------

const initialState = {
  user: null,
  loading: true,
  permissions: {
    is_delete: false,
    is_insert: false,
    is_update: false,
    is_select: false,
  },
};

const reducer = (state, action) => {
  if (action.type === "INITIAL") {
    return {
      loading: false,
      user: action.payload.user,
    };
  }
  if (action.type === "LOGIN") {
    return {
      ...state,
      user: action.payload.user,
    };
  }
  if (action.type === "REGISTER") {
    return {
      ...state,
      user: action.payload.user,
    };
  }
  if (action.type === "LOGOUT") {
    return {
      ...state,
      user: null,
    };
  }
  if (action.type === "SET_PERMISSIONS") {
    return {
      ...state,
      permissions: action.payload.permissions,
    };
  }

  return state;
};

// ----------------------------------------------------------------------

const STORAGE_KEY = "accessToken";

export function AuthProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  const initialize = useCallback(async () => {
    try {
      const accessToken = await AsyncStorage.getItem("accessToken");

      if (accessToken && isValidToken(accessToken)) {
        setSession(accessToken);

        const response = await axiosGeneral.get(endpoints.auth.me);

        const { email, student_code } = response.data.user;

        dispatch({
          type: "INITIAL",
          payload: {
            user: {
              email,
              student_code,
            },
          },
        });
      } else {
        dispatch({
          type: "INITIAL",
          payload: {
            user: null,
          },
        });
      }
    } catch (error) {
      console.error(error);
      dispatch({
        type: "INITIAL",
        payload: {
          user: null,
        },
      });
    }
  }, []);

  useEffect(() => {
    initialize();
  }, [initialize]);

  // LOGIN
  const login = useCallback(async (data) => {
    try {
      const response = await axiosLogin.post("", data);

      if (response.data.accessToken) {
        const { accessToken } = response.data;

        await setSession(accessToken);

        dispatch({
          type: "LOGIN",
          payload: {
            user: {
              token: accessToken,
            },
          },
        });
        return true;
      }
      throw new Error("No access token received");
    } catch (error) {
      console.error("Login failed:", error);
      throw error;
    }
  }, []);

  // REGISTER
  const register = useCallback(async (email, password, firstName, lastName) => {
    const data = {
      email,
      password,
      firstName,
      lastName,
    };

    const response = await axios.post(endpoints.auth.register, data);

    const { accessToken, user } = response.data;

    sessionStorage.setItem(STORAGE_KEY, accessToken);

    dispatch({
      type: "REGISTER",
      payload: {
        user: {
          ...user,
          accessToken,
        },
      },
    });
  }, []);

  // SENDING OTP
  const sendOTP = useCallback(async (req_body) => {
    try {
      const response = await axiosGeneral.post(endpoints.auth.otp, {
        ...req_body,
      });

      return response;
    } catch (error) {
      console.error("Failed to send OTP:", error);
      throw error;
    }
  }, []);

  // LOGOUT
  const logout = useCallback(async () => {
    setSession(null);
    dispatch({
      type: "LOGOUT",
    });
  }, []);

  // SET CURRENT MENU PERMISSIONS
  const setCurrentMenuPermissions = useCallback(async (id) => {
    const URL = endpoints.auth.info(id);
    try {
      const response = await axiosGeneral.get(URL);

      const {
        MenuPermissions: { is_delete, is_select, is_update, is_insert },
      } = response.data;

      dispatch({
        type: "SET_PERMISSIONS",
        payload: {
          permissions: {
            is_delete,
            is_update,
            is_insert,
            is_select,
          },
        },
      });
    } catch (error) {
      console.error(error);

      dispatch({
        type: "SET_PERMISSIONS",
        payload: {
          permissions: {
            is_delete: false,
            is_update: false,
            is_insert: false,
            is_select: false,
          },
        },
      });
    }
  }, []);

  // ----------------------------------------------------------------------

  const checkAuthenticated = state.user ? "authenticated" : "unauthenticated";

  const status = state.loading ? "loading" : checkAuthenticated;

  const memoizedValue = useMemo(
    () => ({
      user: state.user,
      permissions: state.permissions,
      method: "jwt",
      loading: status === "loading",
      authenticated: status === "authenticated",
      unauthenticated: status === "unauthenticated",
      //
      login,
      register,
      logout,
      setCurrentMenuPermissions,
      sendOTP,
    }),
    [
      login,
      logout,
      register,
      state.user,
      status,
      state.permissions,
      setCurrentMenuPermissions,
      sendOTP,
    ]
  );

  return (
    <AuthContext.Provider value={memoizedValue}>
      {children}
    </AuthContext.Provider>
  );
}

AuthProvider.propTypes = {
  children: PropTypes.node,
};
