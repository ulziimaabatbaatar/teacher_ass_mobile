import React from "react";
import { StyleSheet, View } from "react-native";
import PropTypes from "prop-types";

const Grid = ({ children, style, flex = 1, row, ...props }) => {
  const blockStyle = StyleSheet.flatten([
    flex !== undefined && { flex },
    row && { flexDirection: "row" },
    style,
  ]);

  return (
    <View style={blockStyle} {...props}>
      {children}
    </View>
  );
};

export default Grid;

Grid.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.object,
  flex: PropTypes.number,
  row: PropTypes.bool,
  setIsSignedIn: PropTypes.func,
};
