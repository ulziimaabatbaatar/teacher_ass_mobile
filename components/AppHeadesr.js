import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Badge, Surface, Title, useTheme } from "react-native-paper";
import Feather from "react-native-vector-icons/Feather";
import PropTypes from "prop-types";
import Colors from "../constants/Colors";

const IconSize = 24;

const AppHeader = ({
  style,
  menu,
  onPressMenu,
  back,
  onPressBack,
  title,
  right,
  rightComponent,
  onRightPress,
  optionalBtn,
  optionalBtnPress,
  headerBg,
  iconColor,
  titleAlight,
  optionalBadge,
}) => {
  const { colors, dark } = useTheme();
  const color = dark ? Colors.white : Colors.dark;
  const bgColor = dark ? Colors.dark : Colors.white;
  const backgroundColor = headerBg || colors.background;

  const LeftView = (
    <View style={styles.view}>
      {menu && (
        <TouchableOpacity onPress={onPressMenu}>
          <Feather name="menu" size={IconSize} color={color || iconColor} />
        </TouchableOpacity>
      )}
      {back && (
        <TouchableOpacity onPress={onPressBack}>
          <Feather
            name="arrow-left"
            size={IconSize}
            color={color || iconColor}
          />
        </TouchableOpacity>
      )}
    </View>
  );

  const RightView = rightComponent || (
    <View style={[styles.view, styles.rightView]}>
      {optionalBtn && (
        <TouchableOpacity style={styles.rowView} onPress={optionalBtnPress}>
          <Feather
            name={optionalBtn}
            size={IconSize}
            color={color || iconColor}
          />
          {optionalBadge && <Badge style={styles.badge}>{optionalBadge}</Badge>}
        </TouchableOpacity>
      )}
      {right !== "" && (
        <TouchableOpacity onPress={onRightPress}>
          <Feather name={right} size={IconSize} color={color || iconColor} />
        </TouchableOpacity>
      )}
    </View>
  );

  const TitleView = (
    <View style={styles.titleView}>
      <Title style={{ color: color || iconColor, textAlign: titleAlight }}>
        {title}
      </Title>
    </View>
  );
  return (
    <Surface style={[styles.header, style, { backgroundColor }]}>
      <LeftView />
      <TitleView />
      <RightView />
    </Surface>
  );
};

AppHeader.propTypes = {
  onPressMenu: PropTypes.func,
  back: PropTypes.func,
  onPressBack: PropTypes.func,
  title: PropTypes.string,
  right: PropTypes.func,
  rightComponent: PropTypes.object,
  onRightPress: PropTypes.func,
  optionalBtn: PropTypes.string,
  optionalBtnPress: PropTypes.func,
  headerBg: PropTypes.string,
  iconColor: PropTypes.string,
  titleAlight: PropTypes.string,
  optionalBadge: PropTypes.string,
  style: PropTypes.array,
  menu: PropTypes.array,
};

export default AppHeader;

const styles = StyleSheet.create({
  badge: {
    position: "absolute",
    right: -10,
    top: -5,
  },

  header: {
    alignItems: "center",
    elevation: 4,
    flexDirection: "row",
    height: 50,
    justifyContent: "space-between",
  },
  rightView: {
    justifyContent: "flex-end",
  },
  rowView: {
    alignItems: "center",
    flexDirection: "row",
    marginRight: 10,
  },
  titleView: {
    flex: 1,
  },
  view: {
    alignItems: "center",
    flexDirection: "row",
    marginHorizontal: 16,
  },
});
